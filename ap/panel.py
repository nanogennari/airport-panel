from . import app, CONFIGS
from .datamodels import Settings

import math
from datetime import datetime, timezone

import requests
import numpy as np
from icecream import ic
from PythonMETAR import Metar
from PythonMETAR.metar import NOAAServError
from flask import render_template, abort

UV_LEVELS = {
    0: "Low",
    1: "Low",
    2: "Low",
    3: "Moderate",
    4: "Moderate",
    5: "Moderate",
    6: "High",
    7: "High",
    8: "Very High",
    9: "Very High",
    10: "Very High",
}

def get_dew_point_c(t_air_c, rel_humidity):
    """Compute the dew point in degrees Celsius
    :param t_air_c: current ambient temperature in degrees Celsius
    :type t_air_c: float
    :param rel_humidity: relative humidity in %
    :type rel_humidity: float
    :return: the dew point in degrees Celsius
    :rtype: float
    """
    A = 17.27
    B = 237.7
    alpha = ((A * t_air_c) / (B + t_air_c)) + math.log(rel_humidity/100.0)
    return (B * alpha) / (A - alpha)

def get_conf(icao):
    if hasattr(CONFIGS, icao):
        conf = getattr(CONFIGS, icao)
    else:
        conf = CONFIGS.default
    return conf

def get_data(icao):
    conf = get_conf(icao)
    if conf.aisweb:
        rotaer = conf.aisweb.get_rotaer(icao)
        sun = conf.aisweb.get_sun(icao)
    elif CONFIGS.default.aisweb:
        rotaer = CONFIGS.default.aisweb.get_rotaer(icao)
        sun = CONFIGS.default.aisweb.get_sun(icao)
    else:
        out = {
            'sunset': "NO DATA",
            'sunrise': "NO DATA",
            'runways': [],
            'lat': 0,
            'lon': 0,
            'auto_update': -1,
            'extra_html': "",
            'pres_str': "NO DATA",
            'wind_str': "NO DATA",
        }
        return out
    if 'error' in rotaer:
        out = {
            'sunset': "NO DATA",
            'sunrise': "NO DATA",
            'runways': [],
            'lat': 0,
            'lon': 0,
            'auto_update': -1,
            'extra_html': "",
            'temp_str': "NO DATA",
            'pres_str': "NO DATA",
            'wind_str': "NO DATA",
            'temp_str': "NO DATA",
            'uv_str': "NO DATA",
            'feels_str': "NO DATA",
            'extra_html': conf.extra_html,
        }
        return out
    if type(rotaer['runways']['runway']) == dict:
        rotaer['runways']['runway'] = [rotaer['runways']['runway']]
    out = {
        'sunrise': f"{sun['sunrise']}Z",
        'sunset': f"{sun['sunset']}Z",
        'runways': [{
            'th1': rw['thr'][0]['ident'],
            'th2': rw['thr'][1]['ident'],
            'string': f"{rw['surface']['#text']} {rw['surface_c']['#text']} {rw['length']['#text']}m",
            } for rw in rotaer['runways']['runway']],
        'lat': rotaer['lat'],
        'lon': rotaer['lng'],
        'temp_str': "NO DATA",
        'pres_str': "NO DATA",
        'wind_str': "NO DATA",
        'temp_str': "NO DATA",
        'uv_str': "NO DATA",
        'feels_str': "NO DATA",
        'map_zoom': conf.map_zoom,
        'extra_html': conf.extra_html,
        'auto_update': conf.auto_update,
    }
    if conf.awn:
        awn_w = conf.awn.get_weather()
        ic(awn_w)
        temp = int(np.round((awn_w['tempf'] - 32)*(5/9)))
        dew = int(np.round((awn_w['dewPoint'] - 32)*(5/9)))
        out['temp_str'] = f"{temp}/{dew} °C"
        out['uv_str'] = f"{awn_w['uv']} - {UV_LEVELS[awn_w['uv']]}"
        #out['feels_str'] = f"{int(np.round((awn_w['feelsLike'] - 32)*(5/9)))} °C"
        out['pres_str'] = f"{int(np.round(awn_w['baromabsin']*33.863889532610884) + conf.awn.pressure_correction)} hPa"
        wind_speed = int(np.round(awn_w['windspdmph_avg10m']*0.868976))
        wind_dir = str(int(np.round(awn_w['winddir_avg10m']/10)*10)).zfill(3)
        gust_speed = int(np.round(awn_w['windgustmph']*0.868976))
        if (gust_speed - wind_speed) >= 5:
            out['wind_str'] = f"{wind_dir} {str(wind_speed).zfill(2)}G{str(gust_speed).zfill(2)}kt"
        else:
            out['wind_str'] = f"{wind_dir} {str(wind_speed).zfill(2)}kt"
        if (gust_speed >= 15) or (wind_speed >= 10):
            out['wind_warning'] = "red"
    elif conf.openweather:
        ow_data = conf.openweather.get_weather(rotaer['lat'], rotaer['lng'])
        ic(ow_data)
        temp = int(np.round(ow_data['main']['temp']))
        dew = int(np.round(get_dew_point_c(ow_data['main']['temp'], ow_data['main']['humidity'])))
        out['temp_str'] = f"{temp}/{dew} °C"
        out['uv_str'] = f"NO DATA"
        #out['feels_str'] = f"{int(np.round(ow_data['main']['feels_like']))} °C"
        out['pres_str'] = f"{int(ow_data['main']['pressure'])} hPa"
        wind_speed = str(int(np.round(ow_data['wind']['speed']*1.94384))).zfill(2)
        wind_dir = str(int(np.round(ow_data['wind']['deg']/10)*10)).zfill(3)
        out['wind_str'] = f"{wind_dir} {wind_speed}kt"
    #ic(out)
    return out

def get_metars(icao):
    conf = get_conf(icao)
    metars = []
    try:
        ic(icao)
        metars = [Metar(icao.upper())]
        ic(metars)
    except NOAAServError:
        pass
    for i in conf.metars:
        try:
            metars.append(Metar(i.upper()))
        except NOAAServError:
            pass
    if len(metars) == 0:
        metars = ["METAR not available!"]
    return metars

@app.route("/<icao>", methods=["GET"])
@app.route("/<icao>/", methods=["GET"])
def entry(icao=""):
    ic(f"Request for {icao}")
    ic(len(icao))
    if len(icao) == 4 and icao.isalpha():
        ic("Responding")
        icao = icao.lower()
        data = get_data(icao)
        data['updated'] = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%SZ")
        return render_template(
            "panel.html",
            icao=icao.upper(),
            data = data,
            metars=get_metars(icao),
        )
    else:
        ic("404")
        abort(404)


@app.route("/", methods=["GET"])
def index():
    if hasattr(CONFIGS, 'default'):
        return render_template("index.html", index_html=CONFIGS.default.index_html)
    else:
        return render_template("index.html", index_html="<div class=\"error\"><div class=\"error-message\">No configuration set!</div></div>")

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    return render_template('500.html', error_message=error, data={'auto_update': 30}), 404