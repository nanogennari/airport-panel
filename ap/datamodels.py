import json
from typing import List

from icecream import ic
import requests
import xmltodict
from pydantic import BaseModel

class AiswebAPI(BaseModel):
    API_KEY: str
    API_PASS: str
    API_URL: str = "https://aisweb.decea.mil.br/api/"

    def _make_request(self, url):
        ic(f"Requesting {url}.")
        response = requests.request("GET", url, headers={}, data={})
        return xmltodict.parse(response.content)['aisweb']

    def get_rotaer(self, icao):
        url = f"{self.API_URL}?apiKey={self.API_KEY}&apiPass={self.API_PASS}&area=rotaer&icaoCode={icao.upper()}"
        return self._make_request(url)

    def get_sun(self, icao):
        url = f"{self.API_URL}?apiKey={self.API_KEY}&apiPass={self.API_PASS}&area=sol&icaoCode={icao.upper()}"
        return self._make_request(url)['day']

class AwnAPI(BaseModel):
    API_KEY: str
    APP_KEY: str
    ENDPOINT: str = "https://api.ambientweather.net/v1"
    STATION_MAC: str
    pressure_correction: float

    def get_weather(self):
        url = f"{self.ENDPOINT}/devices/{self.STATION_MAC}?apiKey={self.API_KEY}&applicationKey={self.APP_KEY}&limit=1"
        ic(f"Requesting {url}.")
        response = requests.request("GET", url, headers={}, data={})
        return json.loads(response.content)[0]

class OpenweatherAPI(BaseModel):
    API_KEY: str
    ENDPOINT: str = "https://api.openweathermap.org/data/2.5/weather"

    def get_weather(self, lat, lon):
        url = f"{self.ENDPOINT}?units=metric&lat={float(lat):.4f}&lon={float(lon):.4f}&appid={self.API_KEY}"
        ic(f"Requesting {url}.")
        response = requests.request("GET", url, headers={}, data={})
        return json.loads(response.content)

class Settings(BaseModel):
    aisweb: AiswebAPI | None = None
    awn: AwnAPI | None = None
    openweather: OpenweatherAPI | None = None
    latitude: float = 0
    longitude: float = 0
    map_zoom: int = 6
    metars: List[str] = []
    extra_html: str = ""
    auto_update: int = -1
    index_html: str = ""