from .datamodels import Settings

import os
import json
from pathlib import Path
from datetime import timedelta
from dataclasses import dataclass

from icecream import ic
from flask import Flask
from flask_session import Session

debug = os.getenv("DEBUG")
debug = debug or "false"
DEBUG = debug.lower() == "true"

if DEBUG:
    ic.configureOutput(
        prefix='DEBUG -> ',
        includeContext=True,
    )
else:
    ic.disable()

SECRET_KEY = os.getenv("SECRET_KEY", "6DUUwdKqwkaXPvjqCS4y")
SESSION_TYPE = "filesystem"
SESSION_PERMANENT = True
SESSION_USE_SIGNER = False
PERMANENT_SESSION_LIFETIME = timedelta(days=30)

app = Flask(__name__)
app.config["DEBUG"] = DEBUG
# To not receive RuntimeError talking that ths session is unavailable beaceuse no secret key was set.
app.config["SESSION_TYPE"] = SESSION_TYPE
app.secret_key = SECRET_KEY
Session(app)
ic("Started app!")

# Load configs files
@dataclass
class CONFIGS:
    pass
confs_folder = Path(app.root_path).parent.joinpath('configs')
if confs_folder.is_dir():
    # List all files in the configs folder, open only json
    for fpath in confs_folder.glob("*.*"):
        if fpath.parts[-1].lower().endswith('.json'):
            with open(fpath, 'r') as f:
                try:
                    conf = json.load(f)
                    if fpath.name.lower() in ("airportpanel.json", "default.json", "config.json", "settings.json"):
                        setattr(CONFIGS, "default", Settings(**conf))
                        print(f"Loaded main config from {fpath.name}")
                    else:
                        setattr(CONFIGS, fpath.stem.lower(), Settings(**conf))
                        print(f"Loaded airport config from {fpath.name}")
                except Exception as error:
                    print(f"Error loading {fpath.name}: {error}")
else:
    print("Warning: No configs folder!")

if DEBUG:
    ic("Loaded configs:")
    for attr in dir(CONFIGS):
        if not(attr.startswith("_")):
            ic(f"{attr}: {getattr(CONFIGS, attr)}")

from . import panel
