# Airport Panel

A dashboard for small airports.

*This software was developed in Brazil and uses mostly local APIs to fetch airport data.*

![Airport Panel](demo.png)

## Instaling

With docker and git installed run:

```bash
git clone https://gitlab.com/nanogennari/airport-panel.git
cd airport-panel
```

Create the configuration files, and:

```bash
docker compose up
```

## Config

Configuration is done by creating the folder `configs` and `JSON` files inside it.

The main configuration is done in the `default.json` file, here a sample config:

```json
{
    "aisweb": {
        "API_KEY": "api_key",
        "API_PASS": "api_pass"
    },
    "openweather": {
        "API_KEY": "api_key"
    },
    "map_zoom": 7,
    "auto_update": -1,
    "index_html": "html with the content for the index page",
    "extra_html": "html to be put at the end of the page"

}
```

And airport specific settings (including custom API keys) can be done in flies `{ICAO_CODE}.json`, for example the `SNPA.json`:

```json
{
    "hide_ads": true,
    "latitude": -19.8425,
    "longitude": -44.600833,
    "map_zoom": 7,
    "auto_update": 300,
    "awn": {
        "API_KEY": "api_key",
        "APP_KEY": "app_key",
        "STATION_MAC": "mac"
    },
    "metars": [
        "SBBH",
        "SBCF",
        "SNDV",
        "SBVG",
        "SBBQ",
        "SBPC",
        "SBAX",
        "SNPD",
        "SBMK",
        "SBIP"
    ],
    "extra_html": "html to be put at the end of the page"

}
```

*All the presented settings can be used globaly in the `default.conf` or in airport specific config.*