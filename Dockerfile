FROM python:3.11-slim

EXPOSE 80

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
RUN python -m pip install poetry

WORKDIR /app
COPY . /app
RUN python -m poetry install

USER root

RUN export SECRET_KEY=`python -c 'import secrets; print(secrets.token_hex())'`

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["poetry", "run", "gunicorn", "--bind", "0.0.0.0:80", "ap:app"]
